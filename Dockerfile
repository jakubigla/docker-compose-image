FROM docker:latest

ENV DOCKER_COMPOSE_VERSION 1.23.2

RUN apk update && apk add py-pip alpine-sdk && \
    pip install docker-compose==${DOCKER_COMPOSE_VERSION}